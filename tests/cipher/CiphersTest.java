package cipher;

import interfaces.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

public class CiphersTest {

    @Test
    public void doCrypt1() {
        Ciphers ciphers = new Ciphers("Encrypt", "hi, my name is egor", Constants.MORSE_ID, "");
        String expected = ".... .. .-.-.-   -- -.--   -. .- -- .   .. ...   . --. --- .-. ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt2() {
        Ciphers ciphers = new Ciphers("Encrypt", "What a wonderful world?", Constants.MORSE_ID, "");
        String expected = ".-- .... .- -   .-   .-- --- -. -... . .-. ..-. ..- .-..   .-- --- .-. .-.. -... ? ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt3() {
        Ciphers ciphers = new Ciphers("Encrypt", "Somebody... rock my car!", Constants.MORSE_ID, "");
        String expected = "... --- -- . -... --- -... -.-- ...... ...... ......   .-. --- -.-. -.-   -- -.--   -.-. .- .-. --..-- ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt4() {
        Ciphers ciphers = new Ciphers("Encrypt", ".,;:!@", Constants.MORSE_ID, "");
        String expected = "...... .-.-.- -.-.-. ---... --..-- .--.-. ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt5() {
        Ciphers ciphers = new Ciphers("Decrypt", "...... .-.-.- -.-.-. ---... --..-- .--.-. ", Constants.MORSE_ID, "");
        String expected = ".,;:!@";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt6() {
        Ciphers ciphers = new Ciphers("Decrypt", "... --- -- . -... --- -... -.-- ...... ...... ......   .-. --- -.-. -.-   -- -.--   -.-. .- .-. --..-- ", Constants.MORSE_ID, "");
        String expected = "somebody... rock my car!";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt7() {
        Ciphers ciphers = new Ciphers("Decrypt", ".-- .... .- -   .-   .-- --- -. -... . .-. ..-. ..- .-..   .-- --- .-. .-.. -...  ", Constants.MORSE_ID, "");
        String expected = "what  a  wonderful  world";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCryptNumbs() {
        Ciphers ciphers = new Ciphers("Encrypt", "1234567890", Constants.MORSE_ID, "");
        String expected = ".---- ..--- ...-- ....- ..... -.... --... ---.. ----. ----- ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCryptNumbsDecrypt() {
        Ciphers ciphers = new Ciphers("Decrypt", ".---- ..--- ...-- ....- ..... -.... --... ---.. ----. ----- ", Constants.MORSE_ID, "");
        String expected = "1234567890";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt8() {
        Ciphers ciphers = new Ciphers("Encrypt", "testing phrase...", Constants.UNICODE_ID, "");
        String expected = "U+0074 U+0065 U+0073 U+0074 U+0069 U+006E U+0067   U+0070 U+0068 U+0072 U+0061 U+0073 U+0065 U+002E U+002E U+002E ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt9() {
        Ciphers ciphers = new Ciphers("Encrypt", ".,;:!@", Constants.UNICODE_ID, "");
        String expected = "U+002E U+002C U+003B U+003A U+0021 U+0040 ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt10() {
        Ciphers ciphers = new Ciphers("Encrypt", "Raptors was a very powerful dinosaurs! It was - 45 billion years ago...", Constants.UNICODE_ID, "");
        String expected = "U+0072 U+0061 U+0070 U+007U+0033 U+007F U+0072 U+0073   U+0077 U+0061 U+0073   U+0061   U+0076 U+006U+0034 U+0072 U+0079   U+0070 U+007F U+0077 U+006U+0034 U+0072 U+0066 U+007U+0034 U+006C   U+006U+0033 U+0069 U+006E U+007F U+0073 U+0061 U+007U+0034 U+0072 U+0073 U+0021   U+0069 U+007U+0033   U+0077 U+0061 U+0073   U+002D   U+0033 U+0034   U+0062 U+0069 U+006C U+006C U+0069 U+007F U+006E   U+0079 U+006U+0034 U+0061 U+0072 U+0073   U+0061 U+0067 U+007F U+002E U+002E U+002E ";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt11() {
        Ciphers ciphers = new Ciphers("Decrypt", "U+0069   U+006C U+0069 U+006B U+0065   U+0079 U+007F U+0075 U+0072   U+0077 U+007F U+0072 U+006B   U+006D U+0061 U+006E ", Constants.UNICODE_ID, "");
        String expected = "i  like  your  work  man";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt12() {
        Ciphers ciphers = new Ciphers("Encrypt", "abcdefghi", Constants.CESAR_ID, "1");
        String expected = "bcdefghij";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt13() {
        Ciphers ciphers = new Ciphers("Encrypt", "abcdefghi", Constants.CESAR_ID, "20");
        String expected = "uvwxyzabc";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);

    }

    @Test
    public void doCrypt14() {
        Ciphers ciphers = new Ciphers("Decrypt", "abcdefghi", Constants.CESAR_ID, "20");
        String expected = "ghijklmno";
        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

    @Test
    public void doCrypt15() {
        Ciphers ciphers = new Ciphers("Decrypt", "1234567890", Constants.CESAR_ID, "20");
        String expected = "1234567890";

        String actual = ciphers.doCrypt();
        assertEquals(expected, actual);
    }

}