package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    Parent start;

    @Override
    public void start(Stage primaryStage) throws Exception {
        start = FXMLLoader.load(getClass().getResource("screens/startWindow.fxml"));
        primaryStage.setTitle("Program cipher");
        primaryStage.setScene(new Scene(start, 600, 600));
        primaryStage.show();

    }
    public static void main(String[] args) {

        launch(args);
    }
}
