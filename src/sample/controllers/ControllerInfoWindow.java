package sample.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;


public class ControllerInfoWindow {

    @FXML
    private Button buttonBackToMenu;

    @FXML
    public void onClickBackToMenu() {
        Stage stageClosing = (Stage) buttonBackToMenu.getScene().getWindow();
        stageClosing.close();

    }

}

