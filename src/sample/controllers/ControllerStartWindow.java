package sample.controllers;

import interfaces.Constants;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import workWithFile.WorkWithFile;
import java.io.IOException;
/**
 * @author Ivanchin Egor 17ИТ18
 * Controller start window
 */
public class ControllerStartWindow{
    public RadioButton rbDecrypt;
    public Button buttonInfo;
    public RadioButton rbEnterTextByKeyboard;
    public RadioButton rbEnterTextByFile;
    public RadioButton rbEncrypt;
    public Button buttonGo;
    private String typeOfEnter = Constants.KEYBOARD_ID;
    private String typeOfCrypt = Constants.ENCRYPT_ID;
    public ToggleGroup radioTypeEnterGroup;
    public ToggleGroup radioTypeCryptGroup;
    private String pathFile;
    private ControllerErrorWindow controllerErrorWindow = new ControllerErrorWindow();
    @FXML
    private TextField labelPath;


    public void onClickInfo() throws IOException {
        Stage stage = new Stage();
        Parent root2 ;
        root2 = FXMLLoader.load(getClass().getResource("infoWindow.fxml"));
        stage.setTitle("Info");
        stage.setScene(new Scene(root2, 600, 200));
        stage.show();
    }

    public void onClickEnterTextByKeyboard() {
        labelPath.setDisable(true);
        typeOfEnter = Constants.KEYBOARD_ID;
    }

    public void onClickEnterTextByFile() {
        labelPath.setDisable(false);
        typeOfEnter = Constants.FILE_ID;
    }

    public void onClickEncryptText() { typeOfCrypt = Constants.ENCRYPT_ID; }

    public void onClickDecryptText() { typeOfCrypt = Constants.DECRYPT_ID; }

    public void onClickGo() {
        readPathLabel();
        ControllerCryptWindow controllerCryptWindow = new ControllerCryptWindow();
        try {
            if (verificationChoice(Constants.KEYBOARD_ID)) {
                controllerCryptWindow.createWindow(typeOfCrypt, "");
            }
            if (verificationChoice(Constants.FILE_ID)) {
                if (verificationPath(labelPath.getCharacters().toString())) {
                        controllerCryptWindow.createWindow(typeOfCrypt, WorkWithFile.readFile(pathFile));
                }
            }
        }catch (IOException e) {
            controllerErrorWindow.createWindow(Constants.ERROR_FOUND_FILE);
        } catch (Exception e) {
            controllerErrorWindow.createWindow(Constants.ERROR_UNEXPECTED_ERROR);
        }
    }

    private void readPathLabel() {
        pathFile = labelPath.getText();
    }

    private boolean verificationChoice(String enterType) {
        return (typeOfEnter.equals(enterType));
    }

    private boolean verificationPath(String path) {
        boolean correctness = true;
        if (!path.matches(Constants.PATTERN_PATH)) {
            controllerErrorWindow.createWindow(Constants.ERROR_PATH);
            correctness = false;
        }

        return correctness;
    }
}