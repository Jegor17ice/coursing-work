package sample.controllers;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * @author Ivanchin Egor 17ИТ18
 * Controller error window
 */
public class ControllerErrorWindow {

    public void createWindow( String message ) {
        Stage stage = new Stage();
        Label title = new Label("Error");
        title.setLayoutX(165);
        title.setLayoutY(14);
        title.setFont(Font.font(32));
        Label error = new Label();

        error.setDisable(false);
        error.setLayoutX(29);
        error.setLayoutY(60);
        error.setPrefHeight(200);
        error.setPrefWidth(350);
        error.setText(message);

        Button buttonCrypt = new Button("okay");
        buttonCrypt.setDisable(false);
        buttonCrypt.setLayoutY(313);
        buttonCrypt.setLayoutX(165);
        buttonCrypt.setPrefHeight(25);
        buttonCrypt.setPrefWidth(70);

        buttonCrypt.setOnAction(event -> {
            Stage stageClosing = (Stage) buttonCrypt.getScene().getWindow();
            stageClosing.close();
        });

        Group group = new Group(title, error, buttonCrypt);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.setWidth(400);
        stage.setHeight(400);
        stage.setTitle("Error");
        stage.show();
    }

}
