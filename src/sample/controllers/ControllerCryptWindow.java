package sample.controllers;

import dataBase.DataBase;
import interfaces.Constants;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import cipher.Ciphers;

import java.sql.SQLException;

/**
 * @author Ivanchin Egor 17ИТ18
 * Controller crypt window
 */
public class ControllerCryptWindow {
    ToggleGroup toggleGroupRB = new ToggleGroup();
    private static String typeOfCipher = Constants.MORSE_ID;
    private TextArea cipherKey = new TextArea();
    private static TextArea textAreaOut = new TextArea();
    private static TextArea textAreaEnter = new TextArea();
    private Button buttonCrypt = new Button();
    private Button buttonCryptAgain = new Button();
    private Button buttonSaveState = new Button();
    private ControllerErrorWindow controllerErrorWindow = new ControllerErrorWindow();
    private String cryptType;
    private String cryptText;

    public void createWindow(String cryptType, String inputText) {
        this.cryptType = cryptType;
        Stage stage = new Stage();

        Label title = new Label("Program Cipher");
        title.setLayoutX(338);
        title.setLayoutY(14);
        title.setFont(Font.font(32));

        textAreaEnter.setDisable(false);
        textAreaEnter.setLayoutX(29);
        textAreaEnter.setLayoutY(91);
        textAreaEnter.setPrefHeight(276);
        textAreaEnter.setPrefWidth(835);
        textAreaEnter.setText(inputText);

        textAreaOut.setDisable(true);
        textAreaOut.setLayoutX(29);
        textAreaOut.setLayoutY(443);
        textAreaOut.setPrefHeight(276);
        textAreaOut.setPrefWidth(835);
        textAreaOut.setText("");

        cipherKey.setLayoutX(766);
        cipherKey.setLayoutY(390);
        cipherKey.setPrefHeight(15);
        cipherKey.setPrefWidth(20);
        cipherKey.setDisable(true);

        RadioButton rbMorseCipher = new RadioButton();
        rbMorseCipher.setLayoutY(398);
        rbMorseCipher.setLayoutX(72);
        rbMorseCipher.setText(Constants.MORSE_ID);
        rbMorseCipher.setSelected(true);
        rbMorseCipher.setToggleGroup(toggleGroupRB);
        rbMorseCipher.setOnAction(event -> {
            cipherKey.setDisable(true);
            typeOfCipher = Constants.MORSE_ID;
            cipherKey.setText("");
        });

        RadioButton rbUnicodeCipher = new RadioButton();
        rbUnicodeCipher.setOnAction(event -> {
            typeOfCipher = Constants.UNICODE_ID;
            cipherKey.setDisable(true);
            cipherKey.setText("");
        });
        rbUnicodeCipher.setLayoutY(398);
        rbUnicodeCipher.setLayoutX(210);
        rbUnicodeCipher.setText(Constants.UNICODE_ID);
        rbUnicodeCipher.setToggleGroup(toggleGroupRB);

        RadioButton rbCesarCipher = new RadioButton();
        rbCesarCipher.setLayoutX(555);
        rbCesarCipher.setLayoutY(398);
        rbCesarCipher.setText(Constants.CESAR_ID);
        rbCesarCipher.setToggleGroup(toggleGroupRB);
        rbCesarCipher.setOnAction(event -> {
            cipherKey.setDisable(false);
            typeOfCipher = Constants.CESAR_ID;
        });

        Label labelKey = new Label();
        labelKey.setLayoutY(398);
        labelKey.setLayoutX(683);
        labelKey.setPrefHeight(17);
        labelKey.setPrefWidth(88);
        labelKey.setText("Shift in Cesar");

        Button buttonBackToMenu = new Button();
        buttonBackToMenu.setText("Back to menu");
        buttonBackToMenu.setLayoutY(46);
        buttonBackToMenu.setLayoutX(50);
        buttonBackToMenu.setOnAction(event -> {
            Stage stageClosing = (Stage) textAreaEnter.getScene().getWindow();
            stageClosing.close();
        });


        buttonCrypt.setDisable(false);
        buttonCrypt.setLayoutY(753);
        buttonCrypt.setLayoutX(345);
        buttonCrypt.setPrefHeight(25);
        buttonCrypt.setPrefWidth(220);
        buttonCrypt.setFont(Font.font(22));
        buttonCrypt.setText(cryptType);
        buttonCrypt.setOnAction(event -> verificationBeforeCrypt());

        buttonCryptAgain.setDisable(true);
        buttonCryptAgain.setLayoutY(753);
        buttonCryptAgain.setLayoutX(94);
        buttonCryptAgain.setPrefHeight(25);
        buttonCryptAgain.setPrefWidth(220);
        buttonCryptAgain.setFont(Font.font(22));
        buttonCryptAgain.setText("Crypt again");
        buttonCryptAgain.setOnAction(event -> refreshItems());

        buttonSaveState.setDisable(true);
        buttonSaveState.setLayoutY(753);
        buttonSaveState.setLayoutX(595);
        buttonSaveState.setPrefHeight(25);
        buttonSaveState.setPrefWidth(220);
        buttonSaveState.setFont(Font.font(22));
        buttonSaveState.setText("Save state");
        buttonSaveState.setOnAction(event -> saveState());

        Group group = new Group(title, textAreaEnter, textAreaOut, rbCesarCipher, rbMorseCipher, rbUnicodeCipher, cipherKey,
                buttonBackToMenu, labelKey, buttonCrypt, buttonCryptAgain, buttonSaveState);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.setWidth(900);
        stage.setHeight(850);
        stage.setTitle(cryptType);
        stage.show();
    }

    private void saveState() {
        ControllerLogBookWindow controllerLogBookWindow = new ControllerLogBookWindow();
        try {
            controllerLogBookWindow.createWindow();
        } catch (SQLException e) {
            controllerErrorWindow.createWindow(Constants.ERROR_LOAD_LOG_BOOK);
        }
    }

    private void refreshItems() {
        textAreaOut.setDisable(true);
        textAreaEnter.setDisable(false);
        textAreaOut.setText("");
        textAreaEnter.setText("");
        cryptText = "";
        buttonCryptAgain.setDisable(true);
        buttonCrypt.setDisable(false);
    }

    private void verificationBeforeCrypt() {
        if (cryptType.equals(Constants.ENCRYPT_ID)) {
            if (verificationKey() && verificationTextEncrypt()) {
                doCrypt();
            }
        }
        if (cryptType.equals(Constants.DECRYPT_ID)) {
            if (verificationKey() && verificationTextDecrypt()) {
                doCrypt();
            }
        }
    }

    private void doCrypt() {
        Ciphers cipher = new Ciphers(cryptType, textAreaEnter.getText(), typeOfCipher, cipherKey.getText());
        textAreaOut.setText("");
        try {
            cryptText = cipher.doCrypt();
            showCryptText();
            sendIntoDataBase();
        } catch (SQLException ignored) {
        } catch (Exception e) {
            controllerErrorWindow.createWindow(Constants.ERROR_UNEXPECTED_ERROR);
        }
    }

    private boolean verificationTextEncrypt() {
        boolean correctness = true;
        String textLabel = textAreaEnter.getText();
        if (!textLabel.matches(Constants.PATTERN_TEXT_AREA) | textLabel.matches("")) {
            controllerErrorWindow.createWindow(Constants.ERROR_ENTER_TEXT);
            correctness = false;
        }
        return correctness;
    }

    private boolean verificationTextDecrypt() {
        boolean correctness = true;
        String textLabel = textAreaEnter.getText();
        if (typeOfCipher.equals(Constants.UNICODE_ID)) {
            if (!textLabel.matches(Constants.PATTERN_UNICODE) | textLabel.matches("")) {
                controllerErrorWindow.createWindow(Constants.ERROR_ENTER_UNICODE);
                correctness = false;
            }
        }
        if (typeOfCipher.equals(Constants.MORSE_ID)) {
            if (!textLabel.matches(Constants.PATTERN_MORSE) | textLabel.matches("")) {
                controllerErrorWindow.createWindow(Constants.ERROR_ENTER_MORSE);
                correctness = false;
            }
        }
        if (typeOfCipher.equals(Constants.CESAR_ID)) {
            if (!textLabel.matches(Constants.PATTERN_TEXT_AREA) | textLabel.matches("")) {
                controllerErrorWindow.createWindow(Constants.ERROR_ENTER_TEXT);
                correctness = false;
            }
        }

        return correctness;
    }

    private boolean verificationKey() {
        boolean correctness = true;
        if (!cipherKey.getText().matches(Constants.PATTERN_KEY) && typeOfCipher.equals(Constants.CESAR_ID)) {
            controllerErrorWindow.createWindow(Constants.ERROR_KEY);
            correctness = false;
        }
        return correctness;
    }

    private void showCryptText() {
        textAreaOut.setDisable(false);
        textAreaEnter.setDisable(true);
        textAreaOut.setText(cryptText);
        cryptText = "";
        buttonCrypt.setDisable(true);
        buttonCryptAgain.setDisable(false);
        buttonSaveState.setDisable(false);

    }

    void sendIntoDataBase() throws SQLException {
        DataBase dataBase = new DataBase();
        dataBase.insert(cryptType, typeOfCipher);
    }

}
