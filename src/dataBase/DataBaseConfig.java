package dataBase;

/**
 * @author Ivanchin Egor 17ИТ18
 * Constants to data base
 */
public interface DataBaseConfig {
    String URL = "jdbc:mysql://localhost:3306/cipher_data_base?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    String USERNAME = "root";
    String PASSWORD = "555361257";
    String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
}
