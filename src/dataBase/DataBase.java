package dataBase;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Ivanchin Egor 17ИТ18
 * Work with data base
 */
public class DataBase implements DataBaseConfig {
    private static Connection connection;
    static {
        try {
            connection = getConnection();
        } catch (Exception e) {

        }
    }
    private static Connection getConnection() throws Exception{
        Connection connection;
        Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        return connection;
    }

    public void insert(String cryptType,String cipherType) throws SQLException {
        Date date = new Date();
        String currentDate = date.getYear() + "-" + date.getMonth() + "-" + date.getDay();
        String query = "insert into list_steps(data, cryptType, cipherType) values("
                + "\""+currentDate + "\","
                + "\""+cryptType + "\","
                + "\""+cipherType
                + "\");";
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    public List<String> select() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "select * from list_steps;";
        ResultSet resultSet = connection.createStatement().executeQuery(query);

        while(resultSet.next()){
            String data = "ID: " + resultSet.getInt("ID")+ " / " +
                    "Data: " + resultSet.getString("data")+ " / " +
                    "Crypt Type: " + resultSet.getString("cryptType")+ " / " +
                    "Cipher Type: " + resultSet.getString("cipherType")+ " ; " ;
            result.add(data);
        }
        return result;
    }
    public void closeConnection() throws SQLException {
        connection.close();
    }
}