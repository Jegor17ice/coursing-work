package workWithFile;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * @author Ivanchin Egor 17ИТ18
 * realization work with file
 */
public class WorkWithFile {
    public static String readFile(String way) throws IOException {
        return new String(Files.readAllBytes(Paths.get(way)), StandardCharsets.UTF_8);
    }
}
