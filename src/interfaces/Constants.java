package interfaces;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivanchin Egor 17ИТ18
 * Constants which use in code
 */
public interface Constants {
    Map<String, String> morseMap = new HashMap<>();
    Map<String, String> uniCodeMap = new HashMap<>();
    String[] alphabetEn = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    String CESAR_ID = "Cesar Cipher";
    String MORSE_ID = "Morse Cipher";
    String UNICODE_ID = "Unicode Cipher";

    String ENCRYPT_ID = "Encrypt";
    String DECRYPT_ID = "Decrypt";

    String KEYBOARD_ID = "keyboard";
    String FILE_ID = "file";

    String PATTERN_PATH = "[A-Z]:(\\\\[\\\\a-zA-Z0-9_]+)+.txt";
    String PATTERN_TEXT_AREA = "[A-Za-z0-9\\.\\,\\\\\\;\\:\\-\\_\\!\\@\\\"\\n\\s\\t\\r]*";
    String PATTERN_CESAR = "[0-9\\s]*[\\.\\,\\\\\\;\\:\\-\\_\\!\\@\\\"]*[\\n\\s\\t\\r]*";
    String PATTERN_KEY = "^([1-9]|[1-2][\\d]|2[0-6])$";
    String PATTERN_MORSE = "[\\.\\- ]*[\\n\\s\\t\\r]*";
    String PATTERN_UNICODE = "([U ]*\\+[0-9A-Za-z ]*)*";

    String ERROR_PATH = "Please, enter correctness absolute path";
    String ERROR_KEY = "Please, enter key with numbers from 0 to 26";
    String ERROR_ENTER_TEXT = "Please use only english alphabet, numbers from 0 to 9 \n" +
                        "and next special symbols : .,;:-_!@\" \n"+
                       "Example: Hi. My name is Egor, I am 18!";
    String ERROR_ENTER_MORSE = "Please use only (.) and (-). Split every letter by space.  \n" +
            "Example: .... ..   -- . -. ";
    String ERROR_ENTER_UNICODE = "Please use only code from a single unicode table. \n" +
            "Example: U+0068 U+0069   U+006D U+0065 U+006E U+002C";
    String ERROR_FOUND_FILE = "File not found";
    String ERROR_LOAD_LOG_BOOK = "error from loading log book";
    String ERROR_UNEXPECTED_ERROR = "something wrong happened";

    static void setMorseVocabularyEncrypt() {
        morseMap.put("", "");
        morseMap.put(" ", " ");
        morseMap.put("\n", "\n");
        morseMap.put("a", ".-");
        morseMap.put("b", "-...");
        morseMap.put("c", "-.-.");
        morseMap.put("d", "-...");
        morseMap.put("e", ".");
        morseMap.put("f", "..-.");
        morseMap.put("g", "--.");
        morseMap.put("h", "....");
        morseMap.put("i", "..");
        morseMap.put("j", ".---");
        morseMap.put("k", "-.-");
        morseMap.put("l", ".-..");
        morseMap.put("m", "--");
        morseMap.put("n", "-.");
        morseMap.put("o", "---");
        morseMap.put("p", ".--.");
        morseMap.put("q", "--.-");
        morseMap.put("r", ".-.");
        morseMap.put("s", "...");
        morseMap.put("t", "-");
        morseMap.put("u", "..-");
        morseMap.put("v", "...-");
        morseMap.put("w", ".--");
        morseMap.put("x", "-..-");
        morseMap.put("y", "-.--");
        morseMap.put("z", "--..");
        morseMap.put("1", ".----");
        morseMap.put("2", "..---");
        morseMap.put("3", "...--");
        morseMap.put("4", "....-");
        morseMap.put("5", ".....");
        morseMap.put("6", "-....");
        morseMap.put("7", "--...");
        morseMap.put("8", "---..");
        morseMap.put("9", "----.");
        morseMap.put("0", "-----");
        morseMap.put(",", ".-.-.-");
        morseMap.put(":", "---...");
        morseMap.put(";", "-.-.-.");
        morseMap.put("\"", ".-..-.");
        morseMap.put("-", "-....-");
        morseMap.put("\\", "-..-.");
        morseMap.put("_", "..--.-");
        morseMap.put("!", "--..--");
        morseMap.put("@", ".--.-.");
    }

    static void setMorseVocabularyDecrypt() {
        morseMap.put("", " ");
        morseMap.put(" ", " ");
        morseMap.put("\n", "\n");
        morseMap.put(".-", "a");
        morseMap.put("-..", "b");
        morseMap.put("-.-.", "c");
        morseMap.put("-...", "d");
        morseMap.put(".", "e");
        morseMap.put("..-.", "f");
        morseMap.put("--.", "g");
        morseMap.put("....", "h");
        morseMap.put("..", "i");
        morseMap.put(".---", "j");
        morseMap.put("-.-", "k");
        morseMap.put(".-..", "l");
        morseMap.put("--", "m");
        morseMap.put("-.", "n");
        morseMap.put("---", "o");
        morseMap.put(".--.", "p");
        morseMap.put("--.-", "q");
        morseMap.put(".-.", "r");
        morseMap.put("...", "s");
        morseMap.put("-", "t");
        morseMap.put("..-", "u");
        morseMap.put("...-", "v");
        morseMap.put(".--", "w");
        morseMap.put("-..-", "x");
        morseMap.put("-.--", "y");
        morseMap.put("--..", "z");
        morseMap.put(".----", "1");
        morseMap.put("..---", "2");
        morseMap.put("...--", "3");
        morseMap.put("....-", "4");
        morseMap.put(".....", "5");
        morseMap.put("-....", "6");
        morseMap.put("--...", "7");
        morseMap.put("---..", "8");
        morseMap.put("----.", "9");
        morseMap.put("-----", "0");
        morseMap.put("......", ".");
        morseMap.put(".-.-.-", ",");
        morseMap.put("---...", ":");
        morseMap.put("-.-.-.", ";");
        morseMap.put(".-..-.", "\"");
        morseMap.put("-....-", "-");
        morseMap.put("-..-.", "\\");
        morseMap.put("..--.-", "_");
        morseMap.put("--..--", "!");
        morseMap.put(".--.-.", "@");
    }

    static void setUnicodeCipherEncrypt() {
        uniCodeMap.put(" ", " ");
        uniCodeMap.put("\n", "\n");
        uniCodeMap.put("a", "U+0061");
        uniCodeMap.put("b", "U+0062");
        uniCodeMap.put("c", "U+0063");
        uniCodeMap.put("d", "U+0064");
        uniCodeMap.put("e", "U+0065");
        uniCodeMap.put("f", "U+0066");
        uniCodeMap.put("g", "U+0067");
        uniCodeMap.put("h", "U+0068");
        uniCodeMap.put("i", "U+0069");
        uniCodeMap.put("j", "U+006A");
        uniCodeMap.put("k", "U+006B");
        uniCodeMap.put("l", "U+006C");
        uniCodeMap.put("m", "U+006D");
        uniCodeMap.put("n", "U+006E");
        uniCodeMap.put("o", "U+007F");
        uniCodeMap.put("p", "U+0070");
        uniCodeMap.put("q", "U+0071");
        uniCodeMap.put("r", "U+0072");
        uniCodeMap.put("s", "U+0073");
        uniCodeMap.put("t", "U+0074");
        uniCodeMap.put("u", "U+0075");
        uniCodeMap.put("v", "U+0076");
        uniCodeMap.put("w", "U+0077");
        uniCodeMap.put("x", "U+0078");
        uniCodeMap.put("y", "U+0079");
        uniCodeMap.put("z", "U+007A");
        uniCodeMap.put("1", "U+0030");
        uniCodeMap.put("2", "U+0031");
        uniCodeMap.put("3", "U+0032");
        uniCodeMap.put("4", "U+0033");
        uniCodeMap.put("5", "U+0034");
        uniCodeMap.put("6", "U+0035");
        uniCodeMap.put("7", "U+0036");
        uniCodeMap.put("8", "U+0037");
        uniCodeMap.put("9", "U+0038");
        uniCodeMap.put("0", "U+0039");
        uniCodeMap.put(",", "U+002C");
        uniCodeMap.put(":", "U+003A");
        uniCodeMap.put(";", "U+003B");
        uniCodeMap.put("\"", "U+0022");
        uniCodeMap.put("-", "U+002D");
        uniCodeMap.put("\\", "U+005C");
        uniCodeMap.put("_", "U+005F");
        uniCodeMap.put("!", "U+0021");
        uniCodeMap.put("@", "U+0040");
    }

    static void setUnicodeCipherDecrypt() {
        uniCodeMap.put("", " ");
        uniCodeMap.put(" ", " ");
        uniCodeMap.put("  ", " ");
        uniCodeMap.put("\n", "\n");
        uniCodeMap.put("U+0061", "a");
        uniCodeMap.put("U+0062", "b");
        uniCodeMap.put("U+0063", "c");
        uniCodeMap.put("U+0064", "d");
        uniCodeMap.put("U+0065", "e");
        uniCodeMap.put("U+0066", "f");
        uniCodeMap.put("U+0067", "g");
        uniCodeMap.put("U+0068", "h");
        uniCodeMap.put("U+0069", "i");
        uniCodeMap.put("U+006A", "j");
        uniCodeMap.put("U+006B", "k");
        uniCodeMap.put("U+006C", "l");
        uniCodeMap.put("U+006D", "m");
        uniCodeMap.put("U+006E", "n");
        uniCodeMap.put("U+007F", "o");
        uniCodeMap.put("U+0070", "p");
        uniCodeMap.put("U+0071", "q");
        uniCodeMap.put("U+0072", "r");
        uniCodeMap.put("U+0073", "s");
        uniCodeMap.put("U+0074", "t");
        uniCodeMap.put("U+0075", "u");
        uniCodeMap.put("U+0076", "v");
        uniCodeMap.put("U+0077", "w");
        uniCodeMap.put("U+0078", "x");
        uniCodeMap.put("U+0079", "y");
        uniCodeMap.put("U+007A", "z");
        uniCodeMap.put("U+0030", "1");
        uniCodeMap.put("U+0031", "2");
        uniCodeMap.put("U+0032", "3");
        uniCodeMap.put("U+0033", "4");
        uniCodeMap.put("U+0034", "5");
        uniCodeMap.put("U+0035", "6");
        uniCodeMap.put("U+0036", "7");
        uniCodeMap.put("U+0037", "8");
        uniCodeMap.put("U+0038", "9");
        uniCodeMap.put("U+0039", "0");
        uniCodeMap.put("U+002E", ".");
        uniCodeMap.put("U+002C", ",");
        uniCodeMap.put("U+003A", ":");
        uniCodeMap.put("U+003B", ";");
        uniCodeMap.put("U+0022", "\"");
        uniCodeMap.put("U+002D", "-");
        uniCodeMap.put("U+005C", "\\");
        uniCodeMap.put("U+005F", "_");
        uniCodeMap.put("U+0021", "!");
        uniCodeMap.put("U+0040", "@");
    }
}
