package cipher;

import interfaces.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("StringConcatenationInLoop")
/**
 * @author Ivanchin Egor 17ИТ18
 * Work with data and ciphers
 */
public class Ciphers {

    private String cryptType;
    private int cipherKey;
    private String inputText;
    private String typeOfCipher;

    private String charTextWithSpaces = "";
    private static String cryptText = "";
    private static List<String> charListLetter = new ArrayList<>();
    private List<String> inputCryptSplitBySpaces = new ArrayList<>();

    public Ciphers(String cryptType, String text, String typeOfCipher, String cipherKey) {
        this.cryptType = cryptType;
        this.inputText = text;
        this.typeOfCipher = typeOfCipher;
        try {
            this.cipherKey = Integer.parseInt(cipherKey);
        } catch (Exception ignored) {
        }
        cryptText = "";
    }

    public String doCrypt() {

        if (cryptType.equals(Constants.ENCRYPT_ID)) {
            doEncrypt();
        }
        if (cryptType.equals(Constants.DECRYPT_ID)) {
            doDecrypt();
        }
        setListNull();
        charTextWithSpaces = "";
        return cryptText;
    }

    private void doDecrypt() {

        if (typeOfCipher.equals(Constants.CESAR_ID)) {
            inputText = inputText.toLowerCase();
            splitByOneLetter();
            fromCesarCipher();
        }
        if (typeOfCipher.equals(Constants.MORSE_ID)) {
            splitBySpaces();
            fromMorseCipher();
        }
        if (typeOfCipher.equals(Constants.UNICODE_ID)) {
            splitBySpaces();
            fromUniCodeCipher();
        }
    }

    private void doEncrypt() {

        if (typeOfCipher.equals(Constants.CESAR_ID)) {
            inputText = inputText.toLowerCase();
            splitByOneLetter();
            toCesarCipher();
        }
        if (typeOfCipher.equals(Constants.MORSE_ID)) {
            inputText = inputText.toLowerCase();
            splitByOneLetter();
            toMorseCipher();
        }
        if (typeOfCipher.equals(Constants.UNICODE_ID)) {
            inputText = inputText.toLowerCase();
            splitByOneLetter();
            toUniCodeCipher();
        }
    }

    private void toMorseCipher() {
        Constants.setMorseVocabularyEncrypt();
        charTextWithSpaces = charTextWithSpaces.replaceAll("\\.", "......");
        for (String trigger : charListLetter) {
            try {
                charTextWithSpaces = charTextWithSpaces.replace(trigger, Constants.morseMap.get(trigger));
            } catch (Exception ignored) {
            }
        }
        cryptText = charTextWithSpaces;
    }

    private void toCesarCipher() {
        for (String s : charListLetter) {
            for (int j = 0; j < Constants.alphabetEn.length; j++) {
                try {
                    if (s.equals(Constants.alphabetEn[j])) {
                        cryptText = cryptText + Constants.alphabetEn[j + cipherKey];
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    cryptText = cryptText + Constants.alphabetEn[j - (26 - cipherKey)];
                }
            }
            if (s.matches(Constants.PATTERN_CESAR)) {
                cryptText = cryptText + s;
            }
        }
    }

    private void toUniCodeCipher() {
        Constants.setUnicodeCipherEncrypt();
        charTextWithSpaces = charTextWithSpaces.replace(".", "U+002E");
        for (String trigger : charListLetter) {
            try {
                charTextWithSpaces = charTextWithSpaces.replaceAll(trigger, Constants.uniCodeMap.get(trigger));
            } catch (Exception ignored) {
            }
        }
        cryptText = charTextWithSpaces;
    }

    private void fromMorseCipher() {
        Constants.setMorseVocabularyDecrypt();
        for (String inputCryptSplitBySpace : inputCryptSplitBySpaces) {
            try {
                cryptText = cryptText + Constants.morseMap.get(inputCryptSplitBySpace);
            } catch (Exception e) {
                cryptText = cryptText + inputCryptSplitBySpace;
            }

        }
    }


    private void fromCesarCipher() {
        for (String s : charListLetter) {
            for (int j = 0; j < Constants.alphabetEn.length; j++) {
                try {
                    if (s.equals(Constants.alphabetEn[j])) {
                        cryptText = cryptText + Constants.alphabetEn[j - cipherKey];
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    cryptText = cryptText + Constants.alphabetEn[j + (26 - cipherKey)];
                }
            }
            if (s.matches(Constants.PATTERN_CESAR)) {
                cryptText = cryptText + s;
            }
        }
    }

    private void fromUniCodeCipher() {
        Constants.setUnicodeCipherDecrypt();
        for (String inputCryptSplitBySpace : inputCryptSplitBySpaces) {
            try {
                cryptText = cryptText + Constants.uniCodeMap.get(inputCryptSplitBySpace);
            } catch (Exception e) {
                cryptText = cryptText + inputCryptSplitBySpace;
            }
        }
    }

    private void splitByOneLetter() {
        char[] oneLetterText = inputText.toCharArray();
        for (char c : oneLetterText) {
            charListLetter.add(String.valueOf(c));
        }
        for (String letter : charListLetter) {
            charTextWithSpaces = charTextWithSpaces + letter + " ";
        }
    }

    private void splitBySpaces() {
        inputCryptSplitBySpaces = Arrays.asList(inputText.split("\\s"));
        for (String inputCryptSplitBySpace : inputCryptSplitBySpaces) {
            charTextWithSpaces = charTextWithSpaces + inputCryptSplitBySpace + " ";
        }
    }

    private void setListNull() {
        for (int i = 0; i < charListLetter.size(); i++) {
            charListLetter.clear();
        }

    }
}
